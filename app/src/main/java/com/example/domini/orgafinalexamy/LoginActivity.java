package com.example.domini.orgafinalexamy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText mUsernameEditText, mPasswordEditText;
    private Button mRegisterButton, mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mapXMLObjects();
        setLoginButtonListener();
        setRegisterButtonListener();
    }

    private void mapXMLObjects() {
        mUsernameEditText = (EditText) findViewById(R.id.userNameEditText);
        mPasswordEditText = (EditText) findViewById(R.id.passwordEditText);

        mRegisterButton = (Button) findViewById(R.id.registerButton);
        mLoginButton = (Button) findViewById(R.id.loginButton);
    }

    private void setLoginButtonListener() {
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usernameActual = mUsernameEditText.getText().toString();
                String passwordActual = mPasswordEditText.getText().toString();

                String usernameExpected = Preferences.get(PreferencesContract.USER_NAME);
                String passwordExpected = Preferences.get(PreferencesContract.PASSWORD);

                if (usernameActual == null || usernameExpected == null ||
                        !usernameActual.equals(usernameExpected) ||
                        passwordActual == null || passwordExpected == null ||
                        !passwordActual.equals(passwordExpected)) {

                    Toast.makeText(LoginActivity.this, "Invalid credentials", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setRegisterButtonListener() {
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });
    }
}

package com.example.domini.orgafinalexamy.menus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.domini.orgafinalexamy.R;

public class AnimationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        ImageView imageA = (ImageView) findViewById(R.id.image_a);
        ImageView imageB = (ImageView) findViewById(R.id.image_b);

        fadeImage(imageA, imageB);
    }

    private void fadeImage(ImageView a, ImageView b) {
        a.animate()
                .rotationBy(3600)
                .alpha(0f)
                .setDuration(1000)
                .start();

        b.animate()
                .rotationBy(3600)
                .alpha(1f)
                .setDuration(1000);
    }
}

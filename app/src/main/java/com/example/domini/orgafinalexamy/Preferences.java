package com.example.domini.orgafinalexamy;

import java.util.HashMap;
import java.util.Map;

public class Preferences {
    
    private static Map<String, String> map = new HashMap<>();

    static {
        map.put(PreferencesContract.PASSWORD, "test");
    }

    public static void put(String key, String phrase) {
        map.put(key, phrase);
    }

    public static String get(String key) {
        return map.get(key);
    }
}

package com.example.domini.orgafinalexamy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistrationActivity extends AppCompatActivity {

    private EditText mFirstNameEditText, mLastNameEditText, mUsernameEditText, mPasswordEditText;
    private Button mRegisterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mapXMLObjects();
        setRegisterButtonListener();
    }

    private void mapXMLObjects() {
        mFirstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
        mLastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
        mUsernameEditText = (EditText) findViewById(R.id.userNameEditText);
        mPasswordEditText = (EditText) findViewById(R.id.passwordEditText);
        mRegisterButton = (Button) findViewById(R.id.registerButton);
    }

    private void setRegisterButtonListener() {
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = mFirstNameEditText.getText().toString();
                String lastName = mLastNameEditText.getText().toString();
                String username = mUsernameEditText.getText().toString();
                String password = mPasswordEditText.getText().toString();

                if (firstName == null || lastName == null || username == null || password == null ||
                        firstName.isEmpty() || lastName.isEmpty() || username.isEmpty() || password.isEmpty()) {

                    Toast.makeText(RegistrationActivity.this, "Invalid credentials", Toast.LENGTH_SHORT).show();
                    return;
                }

                Preferences.put(PreferencesContract.FIRST_NAME, firstName);
                Preferences.put(PreferencesContract.LAST_NAME, lastName);
                Preferences.put(PreferencesContract.USER_NAME, username);
                Preferences.put(PreferencesContract.PASSWORD, password);

                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}

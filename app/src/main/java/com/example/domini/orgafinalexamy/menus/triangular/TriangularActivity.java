package com.example.domini.orgafinalexamy.menus.triangular;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.domini.orgafinalexamy.R;

public class TriangularActivity extends AppCompatActivity {

    private TextView mAnswerText;
    private EditText mInputText;
    private Button mComputeButton;
    private Button mComputeAllButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_triangular);

        mAnswerText = (TextView) findViewById(R.id.answer_text);
        mInputText = (EditText) findViewById(R.id.input_text);
        mComputeAllButton = (Button) findViewById(R.id.compute_all_button);
        mComputeButton = (Button) findViewById(R.id.compute_button);

        setComputeButtonListener();
        setComputeAllButtonListener();
    }

    private void setComputeButtonListener() {
        mComputeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputStr = mInputText.getText().toString();


                if (!inputStr.matches("\\d+")) {
                    Toast.makeText(getApplicationContext(), "Only numeric values between 1 and 10 allowed!", Toast.LENGTH_SHORT).show();
                    return;
                }

                int input = Integer.parseInt((inputStr));

                if (input < 1 || input > 10) {
                    Toast.makeText(getApplicationContext(), "Only numeric values between 1 and 10 allowed!", Toast.LENGTH_SHORT).show();
                    return;
                }

                mAnswerText.setText(input + " = " + triangular(input) + "");
            }
        });
    }

    private void setComputeAllButtonListener() {
        mComputeAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAnswerText.setText("");
                for (int i = 1; i <= 10; i++) {
                    mAnswerText.setText(mAnswerText.getText().toString() + i + " = " + triangular(i) + "\n");
                }
            }
        });
    }

    int triangular(int input)  {
        if (input ==1)
            return 1;

        return triangular(input-1) + input;
    }
}

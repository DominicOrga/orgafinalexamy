package com.example.domini.orgafinalexamy.menus.atm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.domini.orgafinalexamy.R;

public class ATMRegistrationActivity extends AppCompatActivity {

    private static final String TAG_NAME = ATMRegistrationActivity.class.getSimpleName();

    private EditText mAccountNumberEditText;
    private EditText mAccountBalanceEditText;
    private EditText mAccountPinEditText;
    private Button mRegisterButton;

    private String mAccountNumber;
    private float mAccountBalance;
    private String mAccountPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atmregistration);

        mAccountNumberEditText = (EditText) findViewById(R.id.accountNumberEditText);
        mAccountBalanceEditText = (EditText) findViewById(R.id.accountBalanceEditText);
        mAccountPinEditText = (EditText) findViewById(R.id.accountPinEditText);

        mRegisterButton = (Button) findViewById(R.id.registerButton);

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    mAccountNumber = mAccountNumberEditText.getText().toString();
                    mAccountBalance = Float.valueOf(mAccountBalanceEditText.getText().toString());
                    mAccountPin = mAccountPinEditText.getText().toString();

                    if (mAccountNumber.length() != 0 && mAccountBalance != 0.0f && mAccountPin.length() != 0) {
                        Intent intent = new Intent(ATMRegistrationActivity.this, ATMLoginActivity.class);
                        intent.putExtra(Constants.ACCOUNT_NUMBER, mAccountNumber);
                        intent.putExtra(Constants.ACCOUNT_BALANCE, mAccountBalance);
                        intent.putExtra(Constants.ACCOUNT_PIN, mAccountPin);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    Toast.makeText(ATMRegistrationActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

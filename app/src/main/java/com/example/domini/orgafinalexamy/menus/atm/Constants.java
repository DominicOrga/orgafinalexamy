package com.example.domini.orgafinalexamy.menus.atm;

/**
 * Created by darth on 9/16/17.
 */

/**
 * Extras name used throughout this project
 */
public class Constants {
    static String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    static String ACCOUNT_PASSWORD = "ACCOUNT_PASSWORD";
    static String ACCOUNT_BALANCE = "ACCOUNT_BALANCE";
    static String ACCOUNT_PIN = "ACCOUNT_PIN";
}

package com.example.domini.orgafinalexamy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.domini.orgafinalexamy.menus.AnimationActivity;
import com.example.domini.orgafinalexamy.menus.atm.ATMLoginActivity;
import com.example.domini.orgafinalexamy.menus.triangular.TriangularActivity;

public class MenuActivity extends AppCompatActivity {

    private TextView mNameTextView;

    private Button mPlayAnimationButton, mPlayATMButton, mPlayTriangularButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        setXMLObjects();
        setPlayAnimationButton();
        setPlayATMButton();
        setPlayTriangularButton();
        setName();
    }

    private void setXMLObjects() {
        mPlayAnimationButton = (Button) findViewById(R.id.playAnimationButton);
        mPlayATMButton = (Button) findViewById(R.id.playATMButton);
        mPlayTriangularButton = (Button) findViewById(R.id.playTriangularButton);
        mNameTextView = (TextView) findViewById(R.id.nameTextView);
    }

    private void setName() {
        mNameTextView.setText(String.format("Hello, %s %s",
                Preferences.get(PreferencesContract.FIRST_NAME),
                Preferences.get(PreferencesContract.LAST_NAME))
        );
    }

    private void setPlayAnimationButton() {
        mPlayAnimationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, AnimationActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setPlayATMButton() {
        mPlayATMButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ATMLoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setPlayTriangularButton() {
        mPlayTriangularButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, TriangularActivity.class);
                startActivity(intent);
            }
        });
    }
}
